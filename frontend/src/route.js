/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2019 Remek Zajac
 *************************************************************************/
'use strict';

const malgos = [
    require("./astar.js"),
    require("./thetastar.js")
];

const route = (function() {

    class Route {

        constructor(grid, debugMsCb) {
            const self = this;
            this.grid = grid;
            $("#reroute").click(this.route.bind(this));
            let algosPick = $("#algos");
            let algosPickList = algosPick.find(".dropdown-menu");
            this.algos = malgos.map((algo, idx) => {
                algo = new algo(this);
                algosPickList.append(`<li><a href="#" data-idx="${idx}">${algo.name}</a></li>`);
                return algo;
            });
            let localStorageKey = "pathplanning.algo";
            function pickAlgo(idx) {
                self.algoIdx = parseInt(idx);
                algosPick.find(".dropdown-toggle").html(
                    self.algos[self.algoIdx].name + '<b class="caret"></b>'
                );
                self._renderAlgorithmConfig(self.algos[self.algoIdx].configuration);
                window.localStorage.setItem(localStorageKey, `${self.algoIdx}`);
            }

            algosPickList.find("li").click((e) => {
                pickAlgo(
                    parseInt(e.target.getAttribute("data-idx"))
                );
            });

            pickAlgo(window.localStorage.getItem(localStorageKey) || "0");
            this.debugMsCb = debugMsCb
        }

        _renderAlgorithmConfig(config) {
            let configDiv = $(".navbar-fixed-bottom")
            configDiv.empty();
            for (var ckey in config) {
                let configItem = config[ckey];
                if (configItem.type == "function") {
                    configDiv.append(
                        `<div class="panel panel-default pull-left" style="height:120px; margin-bottom:0">
                            <div class="panel-body" style="padding: 6px;">
                                <div>${configItem.label}</div>
                                <textarea rows="4" cols="40" class="code ${ckey}">${configItem.body}</textarea>
                            </div>
                        </div>`
                    );
                    configDiv.find("."+ckey).change((e) => {
                        configItem.body = e.target.value;
                    });
                }
            }
        }

        start(node) {
            this._start = node;
            if (this.finish) {
                this.route();
            }
        }

        finish(node) {
            this._finish = node;
            if (this.start) {
                this.route();
            }
        }

        route(debugMs) {
            if (this._start && this._finish) {
                this.algos[this.algoIdx].route(
                    this._start,
                    this._finish,
                    this.debugMsCb()
                ).then((route) => {
                    this.drawRoute(route);
                });
                $("#reroute").removeClass("disabled");
            }
        }

        drawRoute(route) {
            this.grid.dynamicContext.clear();
            if (route) {
                this.grid.dynamicContext.strokeStyle = "#00F000";
                this.grid.dynamicContext.beginPath();
                this.grid.dynamicContext.moveTo(route.node.x, route.node.y)
                route = route.prev;
                while(route) {
                    this.grid.dynamicContext.lineTo(route.node.x, route.node.y);
                    route = route.prev;
                }
                this.grid.dynamicContext.stroke();
            }
        }
    }

    return {
        Route : Route
    }

})();

module.exports = route;
