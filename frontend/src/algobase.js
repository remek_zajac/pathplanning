/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2019 Remek Zajac
 *************************************************************************/
'use strict';

const heapm = require("collections/heap");

const algo = (function() {

    class Openset {
        constructor() {
            this.heap = new heapm.Heap(
                [],
                (a,b) => {
                    return a.node.key == b.node.key;
                },
                (a,b) => {
                    return b.F - a.F;
                }
            );
            this.nodes = {};
        }

        push(node) {
            this.nodes[node.node.key] = node;
            this.heap.push(node);
        }

        pop() {
            return this.heap.pop();
        }

        get(key) {
            return this.nodes[key];
        }

        get length() {
            return this.heap.length;
        }
    }

    class RoutingAlgo {
        constructor(name, debugContext) {
            this.name = name;
            this.debugContext = debugContext;
        }

        _config(name, ...rest) {
            let config = this.configuration[name];
            if (config.type == "function") {
                var func = Function.apply(undefined, config.arguments.concat(config.body));
                return func.apply(undefined, rest);
            }
        }
    }

    return {
        Openset : Openset,
        RoutingAlgo : RoutingAlgo
    }

})();

module.exports = algo;
