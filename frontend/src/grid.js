/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2019 Remek Zajac
 *************************************************************************/
'use strict';

const grid = (function() {

    class DrawingContext {

        constructor(canvas, cellSizePx) {
            this.canvas = canvas;
            this.cellSizePx = cellSizePx;
            this.drawingContext = this.canvas.getContext("2d");
        }

        drawPoint(x,y,color) {
            var coords = [x*this.cellSizePx+1, y*this.cellSizePx+1, this.cellSizePx-2, this.cellSizePx-2];
            if (color) {
                this.drawingContext.fillStyle = color;
                this.drawingContext.fillRect(coords[0], coords[1], coords[2], coords[3]);
            } else {
                this.drawingContext.clearRect(coords[0], coords[1], coords[2], coords[3]);
            }
        }

        get width() {
            return Math.floor(this.canvas.width / this.cellSizePx);
        }

        get height() {
            return Math.floor(this.canvas.height / this.cellSizePx);
        }
    }

    class StaticDrawingContext extends DrawingContext {
        constructor(canvas, cellSizePx) {
            super(canvas, cellSizePx);
            this._localStorageKey = "pathplanning.StaticDrawingContext";
            this.cache = JSON.parse(window.localStorage.getItem(this._localStorageKey));
            if (this.cache) {
                for (var key in this.cache) {
                    let colour = this.cache[key];
                    let coords = key.split(",").map((v) => {return parseInt(v);});
                    this.drawPoint(coords[0], coords[1], colour);
                }
            } else {
                this.cache = {};
            }
        }

        drawPoint(x,y,color) {
            super.drawPoint(x,y,color);
            if (color) {
                this.cache[`${x},${y}`] = color;
            } else {
                delete this.cache[`${x},${y}`];
            }
        }

        getPoint(x,y) {
            return this.cache[`${x},${y}`];
        }

        save() {
            window.localStorage.setItem(this._localStorageKey, JSON.stringify(this.cache));
        }

        clear() {
            for (var key in this.cache) {
                let coords = key.split(",").map((v) => {return parseInt(v);});
                this.drawPoint(coords[0], coords[1], undefined);
            }
            this.save();
        }
    }

    class DynamicDrawingContext extends DrawingContext {
        constructor(canvas, cellSizePx) {
            super(canvas, cellSizePx);
            this.half = this.cellSizePx/2;
        }

        get strokeStyle() {
            return this.drawingContext.strokeStyle;
        }

        set strokeStyle(v) {
            this.drawingContext.strokeStyle = v;
        }

        get lineWidth() {
            return this.drawingContext.lineWidth;
        }

        set lineWidth(v) {
            this.drawingContext.lineWidth = v;
        }

        beginPath() {
            this.drawingContext.beginPath();
        }

        moveTo(x,y) {
            this.drawingContext.moveTo(
                x*this.cellSizePx+this.half,
                y*this.cellSizePx+this.half);
        }

        lineTo(x,y) {
            this.drawingContext.lineTo(
                x*this.cellSizePx+this.half,
                y*this.cellSizePx+this.half);
        }

        stroke() {
            this.drawingContext.stroke();
        }

        clear() {
            this.drawingContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }
    }

    const mouseevents = new Set(["click", "mousemove", "mousedown", "mouseup", "mouseleave"]);

    class Node {
        constructor(grid, x, y) {
            this.x = x;
            this.y = y;
            this.key = `${this.x},${this.y}`;
            this.grid = grid;
        }

        neighbours() {
            const self = this;
            return [
                new Node(this.grid, this.x+1, this.y),
                new Node(this.grid, this.x-1, this.y),
                new Node(this.grid, this.x+1, this.y+1),
                new Node(this.grid, this.x-1, this.y+1),
                new Node(this.grid, this.x+1, this.y-1),
                new Node(this.grid, this.x-1, this.y-1),
                new Node(this.grid, this.x, this.y+1),
                new Node(this.grid, this.x, this.y-1)
            ].filter((n) => {
                return n.x >= 0 && n.y >= 0 &&
                       n.x <= self.grid.baseContext.width && n.y <= self.grid.baseContext.height &&
                       self.grid.baseContext.getPoint(n.x, n.y) == undefined;
            });
        }

        //cartesian distance
        distance(other) {
            return Math.sqrt(Math.pow(this.x - other.x, 2) + Math.pow(this.y - other.y, 2));
        }

        //returns the number of cells that one needs to traverse between this and other
        cellDistance(other) {
            return Math.max(
                Math.abs(this.x - other.x),
                Math.abs(this.y - other.y)
            );
        }

        isFree() {
            return !this.grid.baseContext.getPoint(this.x, this.y);
        }
    }

    class Grid2D {

        constructor(canvasStatic, canvasDynamic, cellSizePx) {
            this.canvasStatic = canvasStatic;
            this.canvasDynamic = canvasDynamic;
            this.cellSizePx = cellSizePx;
            var canvasStaticElem = canvasStatic[0];
            var canvasDynamicElem = canvasDynamic[0];

            canvasStaticElem.width = canvasStatic.width();
            canvasStaticElem.height = canvasStatic.height();
            canvasDynamicElem.width = canvasStatic.width();
            canvasDynamicElem.height = canvasStatic.height();
            var ctx = canvasStaticElem.getContext("2d");
            ctx.strokeStyle = "#5F0000";
            ctx.lineWidth = 1;
            for (var x = 0; x < canvasStatic.width(); x+=cellSizePx) {
                ctx.beginPath();
                ctx.moveTo(x, 0);
                ctx.lineTo(x, canvasStatic.height());
                ctx.stroke();
            }

            for (var y = 0; y < canvasStatic.height(); y+=cellSizePx) {
                ctx.beginPath();
                ctx.moveTo(0, y);
                ctx.lineTo(canvasStatic.width(), y);
                ctx.stroke();
            }

            this.baseContext = new StaticDrawingContext(this.canvasStatic[0], this.cellSizePx);
            this.dynamicContext = new DynamicDrawingContext(this.canvasDynamic[0], this.cellSizePx);
        }

        on(events, selector, cb) {
            const self = this;
            if (cb == undefined) {
                cb = selector;
                selector = undefined;
            }
            var overridenCb = function(e) {
                if (mouseevents.has(e.type)) {
                    e.grid = {
                        x : Math.floor((e.clientX - self.canvasStatic[0].getBoundingClientRect().left) / self.cellSizePx),
                        y : Math.floor((e.clientY - self.canvasStatic[0].getBoundingClientRect().top) / self.cellSizePx)
                    }
                }
                cb(e);
            }
            this.canvasDynamic.on(events, selector, overridenCb);
        }

        off(events, selector, cb) {
            this.canvasDynamic.off(events, selector, cb);
        }

        node() {
            let x = arguments[0];
            let y = arguments[1];
            if (arguments.length == 1) {
                y = x.y;
                x = x.x;
            }
            return new Node(this, x, y);
        }

        clear() {
            this.baseContext.clear();
            this.dynamicContext.clear();
        }
    }

    return {
        Grid2D: Grid2D
    }

})();

module.exports = grid;