/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2019 Remek Zajac
 *************************************************************************/
'use strict';

const algobase = require("./algobase.js");

const astar = (function() {

    class AStar extends algobase.RoutingAlgo {

        constructor(debugContext, name) {
            super(name || "A*", debugContext);
            this.configuration = {
                H: {
                    label: "Heuristic to goal (node, goal)",
                    type: "function",
                    arguments: ["node", "goal"],
                    body: "return node.distance(goal);"
                },
                cost: {
                    label: "Cost of traversal from a to b",
                    type: "function",
                    arguments: ["a", "b"],
                    body: "return a.distance(b);"
                }
            }
        }

        async route(start, finish, debugMs) {
            let OPEN = new algobase.Openset();
            OPEN.push({
                node : start,
                G: 0,
                F: this._config("H", start, finish)
            });

            let CLOSED = new Set();
            while (OPEN.length) {
                let current = OPEN.pop();
                if (current.node.key == finish.key) {
                    return current;
                }
                CLOSED.add(current.key);
                if (debugMs) {
                    await new Promise(resolve => setTimeout(resolve, debugMs));
                    this.debugContext.drawRoute(current);
                }
                current.node.neighbours().forEach((neighbour) => {
                    if (CLOSED.has(neighbour.key)) {
                        return;
                    }
                    let newNode = this.visitNeighbour(neighbour, current, finish, OPEN.get(neighbour.key));
                    if (newNode) {
                        OPEN.push(newNode);
                    }
                });
            }
        }

        //neighbour - the neighbour of current to visit
        //finish    - the goal node
        //existing  - existing reference (if any) to the neighbour in the OPEN set
        visitNeighbour(neighbour, current, finish, existing) {
            let G = current.G + this._config("cost", current.node, neighbour);
            if (!existing || G < existing.G) {
                return {
                    node : neighbour,
                    G: G,
                    F: this._config("H", neighbour, finish) + G,
                    prev: current
                }
            }
        }
    }

    return AStar;

})();

module.exports = astar;
