/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2019 Remek Zajac
 *************************************************************************/
const mgrid  = require("./grid.js");
const mroute = require("./route.js");

const map = $("#map");
const staticCanvas = map.find(".static");
const dynamicCanvas = map.find(".dynamic");
const grid = new mgrid.Grid2D(staticCanvas, dynamicCanvas, 16);
const route = new mroute.Route(
    grid,
    () => { //trace callback
        return parseInt($("#traceDelay")[0].value);
    }
);
$("#clearmap").click(() => {
    grid.clear();
});

var popover;
grid.on("click", (e) => {
    if (popover) {
        popover.popover("hide");
        popover = undefined;
        return;
    }
    if (staticCanvas.hasClass("drawing")) {
        return;
    }
    e.stopPropagation();
    map.popover({
        html: true,
        content: '<div><a id="start" href="#">start</a></div>' +
                 '<div><a id="finish" href="#">finish</a></div>',
        placement: "right"
      }).popover('show');

    popover = $(".popover");
    popover.css("left", e.clientX-map[0].getBoundingClientRect().left+"px");
    popover.css("top", e.clientY-map[0].getBoundingClientRect().top-(popover.height()/2)+5+"px");

    popover.find("#start").on("click", () => {
        popover.popover("hide");
        popover = undefined;
        route.start(grid.node(e.grid));
    });
    popover.find("#finish").on("click", () => {
        popover.popover("hide");
        popover = undefined;
        route.finish(grid.node(e.grid));
    });
});


var longpress = undefined;
const obstacleColor = "#FF0000";
var drawColor = obstacleColor;

grid.on("mousedown", (e) => {
    longpress = e.grid;
    setTimeout(() => {
        if (longpress) {
            staticCanvas.addClass("drawing");
            if (grid.baseContext.getPoint(longpress.x, longpress.y)) {
                drawColor = undefined;
            } else {
                drawColor = obstacleColor;
            }
            grid.baseContext.drawPoint(longpress.x, longpress.y, drawColor);
        }
    }, 2000);
});

grid.on("mouseup", (e) => {
    setTimeout(() => {
        if (staticCanvas.hasClass("drawing")) {
            staticCanvas.removeClass("drawing");
            grid.baseContext.save();
        }
    }, 0);

    longpress = undefined;
});

grid.on("mousemove", (e) => {
    if (staticCanvas.hasClass("drawing")) {
        grid.baseContext.drawPoint(e.grid.x, e.grid.y, drawColor);
    }
    longpress = undefined;
});
