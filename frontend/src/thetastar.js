
/*************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * (C) 2019 Remek Zajac
 *************************************************************************/
'use strict';

const Astar = require("./astar.js");

const thetastar = (function() {

    class ThetaStar extends Astar {

        constructor(debugContext) {
            super(debugContext, "Theta*")
        }

        //neighbour - the neighbour of current to visit
        //finish    - the goal node
        //existing  - existing reference (if any) to the neighbour in the OPEN set
        visitNeighbour(neighbour, current, finish, existing) {
            let parentOfCurrent = current.prev;
            if (parentOfCurrent && this.lineOfSight(parentOfCurrent.node, neighbour)) {
                return super.visitNeighbour(neighbour, parentOfCurrent, finish, existing);
            } else {
                return super.visitNeighbour(neighbour, current, finish, existing);
            }
        }

        lineOfSight(nodeA, nodeB) {
            if (nodeA.cellDistance(nodeB) <= 1) {
                return true;
            }
            let midNode = nodeA.grid.node(
                nodeA.x + Math.floor((nodeB.x - nodeA.x)/2),
                nodeA.y + Math.floor((nodeB.y - nodeA.y)/2),
            );
            if (!midNode.isFree()) {
                return false;
            }
            return this.lineOfSight(nodeA, midNode) && this.lineOfSight(midNode, nodeB);
        }
    }

    return ThetaStar;

})();

module.exports = thetastar;
